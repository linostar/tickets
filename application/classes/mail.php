<?php
class Mail {
	private $name;
	private $email;

	public function __construct($name, $email) {
		$this->name = $name;
		$this->email = $email;
	}
	
	public function new_ticket($tid, $hash) {
		$title = 'Rizon Ticket #'.$tid.' Created';
		$message =
'
<p>'.$this->name.',</p>
<p>Your ticket has been entered and is now pending. Any updates will be emailed to you automatically.</p>
<p>To view or reply to your ticket, visit this URL: <a href="'.url::site('akills/view/'.$tid.'/'.$hash, true).'">Ticket #'.$tid.'</a>. Do not reply to this email.</p>
<p style="margin-top: 1em; font-style: italic;">The Rizon Staff Team</p>
';

		$this->send($title, $message);
	}

	public function ticket_replied($tid, $hash, $by, $ticketStatus) {
		$title = 'Rizon Ticket #'.$tid.' ('.$ticketStatus.'): Status Update';
		$message =
'
<p>'.$this->name.',</p>
<p>Your ticket has been replied to by '.$by.'. Any subsequent updates will also be emailed to you automatically.</p>
<p>To view or reply to your ticket, visit this URL: <a href="'.url::site('akills/view/'.$tid.'/'.$hash, true).'">Ticket #'.$tid.'</a>. Do not reply to this email.</p>
<p style="margin-top: 1em; font-style: italic;">The Rizon Staff Team</p>
';

		$this->send($title, $message);
	}
	
	public function ticket_autoclosed($tid) {
		$title = 'Rizon Ticket #'.$tid.' (Closed): Status Update';
		$message =
'
<p>'.$this->name.',</p>
<p>Your ticket has been closed automatically by the ticket system after 72 hours of inactivity. If you have any further issues, please submit a new ticket.</p>
<p style="margin-top: 1em; font-style: italic;">The Rizon Staff Team</p>
';

		$this->send($title, $message);
	}
	
	public function ticket_assigned($tid, $hash, $to) {
		$title = 'Rizon Ticket #'.$tid.' (In-Progress): Status Update';
		$message =
'
<p>'.$this->name.',</p>
<p>Your ticket has been assigned to '.$to.'. Any subsequent updates will also be emailed to you automatically.</p>
<p>To view or reply to your ticket, visit this URL: <a href="'.url::site('akills/view/'.$tid.'/'.$hash, true).'">Ticket #'.$tid.'</a>. Do not reply to this email.</p>
<p style="margin-top: 1em; font-style: italic;">The Rizon Staff Team</p>
';

		$this->send($title, $message);		
	}

	public function ticket_unassigned($tid, $hash) {
		$title = 'Rizon Ticket #'.$tid.' (Pending): Status Update';
		$message =
'
<p>'.$this->name.',</p>
<p>Your ticket has been unassigned and is now pending again. Any subsequent updates will also be emailed to you automatically.</p>
<p>To view or reply to your ticket, visit this URL: <a href="'.url::site('akills/view/'.$tid.'/'.$hash, true).'">Ticket #'.$tid.'</a>. Do not reply to this email.</p>
<p style="margin-top: 1em; font-style: italic;">The Rizon Staff Team</p>
';

		$this->send($title, $message);		
	}
	
	public function ticket_updated($tid) {
		$title = 'Rizon Ticket #'.$tid.' (In-Progress): New Reply';
		$message =
'
<p>'.$this->name.', a ticket that is currently in-progress with you has been replied to by the ticket submitter.</p>
<p><a href="'.url::site('akills/view/'.$tid, true).'">Ticket #'.$tid.'</a>.</p>
';

		$this->send($title, $message);				
	}
	
	public function send($title, $message) {
		if (IN_PRODUCTION) {
			require Kohana::find_file('vendor', 'Swift/swift_required');
			
			$transport = Swift_SmtpTransport::newInstance('outbound.relay.rizon.net', 25);
			$mailer = Swift_Mailer::newInstance($transport);
			$message = Swift_Message::newInstance()
				->setSubject($title)
				->setFrom(array('tickets@rizon.net' => 'Rizon Tickets'))
				->setTo(array($this->email))
				->setBody($message, 'text/html');
			$result = $mailer->send($message);
		}
	}
	
}
?>
