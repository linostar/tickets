<?php 
defined('SYSPATH') or die('No direct script access.');

class Controller_Akills extends Controller_Template {

	public function before() {
		$this->auth = Auth::instance();
		parent::before();
	}

	public function action_index() {
		$this->page_title = 'Akills';
		$this->template->content = View::factory('pages/akills');
	}
	
	public function action_appeal() {
		$this->page_title = 'Appeal Akill';
		
		$banned = new Model_Banned();
		$v = View::factory('pages/akills');
		$v->allowAppeal = $this->canAppeal($_SERVER['REMOTE_ADDR'], $_POST['ip'], $_POST['type']);
		
		if (!isset($_POST['host_number'])) {
			$v->akill = $this->lookupAkill($_POST['ip']);
			if ($v->akill !== false && $v->akill['time'] <= 3600) {
				$v->allowAppeal = false;
			}
		}
		
		
		$validate = new Validate($_POST);
		$validate->rules('ip', array('not_empty'=>array(), 'ip'=>array()));
		$validate->callback('ip', array($this, 'form_ipcheck'), array($validate['ip'], $validate['type']));
		if (ENABLE_RECAPTCHA)
			$validate->callback('recaptcha_response_field', array($this, 'recaptcha'), array($validate['recaptcha_challenge_field']));
		$validate->rules('contact_name', array('not_empty'=>array(), 'max_length'=>array(255)));
		if (IN_PRODUCTION) {
			$validate->rules('contact_email', array('not_empty'=>array(), 'max_length'=>array(255), 'email'=>array(false)));
		}
		if (!isset($v->akill)) {
			$validate->rules('static', array('not_empty'=>array()));
			$validate->rules('host_number', array('not_empty'=>array(), 'numeric'=>array(), 'range'=>array(5, 1000)));
		} else if ($v->akill === false) {
			$validate->rules('ban_message', array('not_empty'=>array(), 'max_length'=>array(65536)));
		}
		$validate->rules('why', array('not_empty'=>array(), 'max_length'=>array(65536)));
		
		
		if (!$validate->check() || !$v->allowAppeal) {
			$v->errors = $validate->errors();
			$v->post = $_POST;
		} else {
			$ticket = new Model_Ticket();
			
			if (isset($_POST['host_number'])) {
				$i = $ticket->add(array(
					'ip' => $_POST['ip'],
					'real_ip' => $_SERVER['REMOTE_ADDR'],
					'contact_name' => $_POST['contact_name'],
					'contact_email' => $_POST['contact_email'],
					'ban_message' => isset($_POST['ban_message'])?$_POST['ban_message']:'',
					'host_number' => $_POST['host_number'],
					'why' => $_POST['why'],
					'akill' => null
				));
			} else {
				$akill = new Model_Akill();
				$ak = $akill->getGeo($_POST['ip']);
	
				if ($ak !== false) {
					$i = array($ak['ip'], $ak['time'], $ak['message'], $ak['addedby'], $ak['timestamp'], $ak['akillid']);
					$akId = $akill->add($i);
				}
				
				$i = $ticket->add(array(
					'ip'=>$_POST['ip'],
					'real_ip'=>$_SERVER['REMOTE_ADDR'],
					'contact_name'=>$_POST['contact_name'],
					'contact_email'=>$_POST['contact_email'],
					'ban_message'=>isset($_POST['ban_message'])?$_POST['ban_message']:'',
					'host_number'=>0,
					'why'=>$_POST['why'],
					'akill' => isset($akId)?$akId[0]:null
				));
			}

			$mail = new Mail($_POST['contact_name'], $_POST['contact_email']);
			$mail->new_ticket($i[0], sha1($_POST['ip'].$i[0]));
			$ticket->addHistory($i[0], $this->auth->getUser(), 'Ticket added.');
			Request::current()->redirect('akills/completed');
		}

		$v->lookup_post = $_POST;
		$this->template->content = $v;
	}

	function lookupAkill($ip) {
		$akill = new Model_Akill();
		$ak = $akill->getGeo($ip);

		if ($ak !== false) {
			return array(
				'time' => $ak['time'],
				'message' => $ak['message'],
				'timestamp' => $ak['timestamp'],
				'akillid' => $ak['akillid']
			);
		} else {
			return false;
		}

	}

	function canAppeal($realip, $ip, $type)
	{
		if (!Controller_Akills::ipcheck($ip, $type))
			return false;

		$banned = new Model_Banned();
		if ($banned->check($realip))
			return false;

		if ($type == 'akill')
		{
			$akill = $this->lookupAkill($ip);
			if ($akill !== false && $akill['time'] <= 3600)
			{
				return false;
			}
		}

		return true;
	}

	public function action_lookup() {
		$this->page_title = 'Lookup Ban';
		$v = View::factory('pages/akills');
		$validate = new Validate($_GET);
		$validate->rules('type', array('not_empty'=>array()));
		$validate->rules('ip', array('not_empty'=>array(), 'ip'=>array()));

		$v->lookup_post = $_GET;
		if (!$validate->check()) {
			$v->lookup_errors = $validate->errors();
		} else {
			$v->allowAppeal = $this->canAppeal($_SERVER['REMOTE_ADDR'], $_GET['ip'], $_GET['type']);
			if ($_GET['type'] === 'akill') {
				$v->akill = $this->lookupAkill($_GET['ip']);
			}

		}
		$this->template->content = $v;
	}
	
	public function recaptcha($v, $b, $c) {
		$r = Recaptcha::instance()->check_answer($c[0], $v['recaptcha_response_field']);
		if (!$r->is_valid) {
			$v->error('recaptcha_response_field', 'Invalid Captcha.');
		}
		return $r->is_valid;
	}
	
	public function form_ipcheck($v, $b, $c) {
		$r = self::ipcheck($b, $c);
		if (!$r) {
			$v->error('recaptcha_response_field', 'Invalid Captcha.');
		}
		return $r;
	}
	
	public static function ipcheck($ip, $type) {
		$ticket = new Model_Ticket();
		$ts = $ticket->getByIP($ip);
		foreach ($ts as $t) {
			if ($t['status'] != 3) {
				if ($t['host_number'] == '0') {
					if ($type === 'akill') {
						return false;
					}
				} else {
					if ($type === 'sli') {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public function action_view($t, $h = null) {
		$this->template->content = $this->view_ticket($t, $h);
	}
	
	private function view_ticket($t, $h = null) {
		$ticket = new Model_Ticket();
		$user = new Model_User();
		$akill = new Model_Akill();

		$ti = $ticket->get($t);

		$noreply = false;
		if (substr($h, 0, 2) === "n!") {
			$h = substr($h, 2);
			$noreply = true;
		}

		if ($this->auth->getUser() === null) {
			if ($h !== sha1($ti['ip'].$t)) {
				$c = Request::current();
				$c->redirect('home/login?referrer='.$c->uri());

			}
		}

		$v = View::factory('pages/akills/ticket');
		$v->ticket = $ti;
		$v->replies = $ticket->replies($t);
		$v->history = $ticket->history($t);
		$v->staff = $user->staff($t);
		$v->associatedTickets = $ticket->getByIP($v->ticket['ip'])->as_array();
		if ($ti['host_number'] == '0') {
			$v->akill = $akill->get($v->ticket['akill']);
		}
		$v->noreply = $noreply;
		$v->hash = $h;

		foreach ($v->associatedTickets as $k => $t) {
			if ($t['ticket'] == $v->ticket['ticket']) {
				unset($v->associatedTickets[$k]);
				break;
			}
		}
		return $v;
	}

	public function action_ban($t, $h = null)
	{
		if ($this->auth->getUser() === null) {
			$c = Request::current();
			$c->redirect('home/login?referrer='.$c->uri());
		}

		$ticket = new Model_Ticket();
		$ti = $ticket->get($t);

		$banned = new Model_Banned();
		$banned->ban($ti['real_ip']);

		$ticket->addHistory($t, $this->auth->getUser(), 'Banned user\'s IP '.$ti['real_ip'].'.');

		$this->template->content = "Banned " . $ti['real_ip'];
	}

	public function action_unban($t, $h = null)
	{
		if ($this->auth->getUser() === null) {
			$c = Request::current();
			$c->redirect('home/login?referrer='.$c->uri());
		}

		$ticket = new Model_Ticket();
		$ti = $ticket->get($t);

		$banned = new Model_Banned();
		$banned->unban($ti['real_ip']);

		$ticket->addHistory($t, $this->auth->getUser(), 'Unbanned user\'s IP '.$ti['real_ip'].'.');

		$this->template->content = "Unbanned " . $ti['real_ip'];
	}

	public function action_remove($t, $h = null)
	{
		if ($this->auth->getUser() === null) {
			$c = Request::current();
			$c->redirect('home/login?referrer='.$c->uri());
		}

		$ticket = new Model_Ticket();
		$ticket->remove($t);

		$this->template->content = "Removed ticket " . $t;
	}

	public function action_reply($t, $h = null) {

		$ticket = new Model_Ticket();
		$ti = $ticket->get($t);

		if ($this->auth->getUser() === null) {
			if ($h !== sha1($ti['ip'].$t) || $ti['status'] == 3) {
				$c = Request::current();
				$c->redirect('home/login?referrer='.$c->uri());
			}
		}
		
		$validate = new Validate($_POST);
		$validate->rules('message', array('not_empty' => array(), 'max_length' => array(65536)));
		if (!$validate->check()) {
			$v = $this->view_ticket($t, $h);
			$v->errors = $validate->errors();
			$v->post = $_POST;
			$this->template->content = $v;
		} else {
			$ticket->reply($t, $this->auth->getUser(), $_POST['message']);
			switch ($_POST['submit']) {
				case 'Re-open':
					$_POST['status'] = 0;
					break;
				case 'Re-open and assign to self':
				case 'Reply':
					$_POST['status'] = 1;
					break;
				case 'Resolve':
				case 'Keep resolved':
					$_POST['status'] = 2;
					break;
				case 'Close':
				case 'Keep closed':
					$_POST['status'] = 3;
					break;
				default:
					$_POST['status'] = $ti['status'];
			}

			if ($this->auth->getUser() !== null && $ti['status'] != $_POST['status']) { // staff trying to switch status
				if ($_POST['status'] == 1) {
					$ticket->setStatus($t, $_POST['status']);
					$ticket->assign($t, $this->auth->getUser());
					$ticket->addHistory($t, $this->auth->getUser(), 'Ticket replied to, assigned to self, and status set to '.Ticket::status($_POST['status']).'.');
				} else {
					$ticket->setStatus($t, $_POST['status']);
					$ticket->assign($t, null);
					$ticket->addHistory($t, $this->auth->getUser(), 'Ticket replied to and status set to '.Ticket::status($_POST['status']).'.');
				}
			} else if ($ti['status'] != $_POST['status'] && $ti['status'] == 2 && ($_POST['status'] == 0 || $_POST['status'] == 3)) { // user trying to switch status (only allowed from Resolved -> Pending or Closed)
				$ticket->setStatus($t, $_POST['status']);
				$ticket->assign($t, $this->auth->getUser());
				$ticket->addHistory($t, $this->auth->getUser(), 'Ticket replied to, and status set to '.Ticket::status($_POST['status']).'.');
			} else {
				$ticket->addHistory($t, $this->auth->getUser(), 'Ticket replied to.');
			}

			if ($this->auth->getUser() !== null) { // when a staff member replies
				$mail = new Mail($ti['contact_name'], $ti['contact_email']);
				$mail->ticket_replied($t, sha1($ti['ip'].$t), $this->auth->getUserName(), Ticket::status($_POST['status'])); // email the ticket submitter
			} else { // when a ticket submitter replies
				$newTi = $ticket->get($t); // get updated ticket information
				if ($newTi['status'] == 1) { // if still in-progress
					$user = new Model_User();
					$assigneeEmail = $user->vb3Email($newTi['assignee_name']);
					if ($assigneeEmail !== false) { // and the assignee has a valid email
						$mail = new Mail($newTi['assignee_name'], $assigneeEmail);
						$mail->ticket_updated($t); // email the assignee
					}
				}
			}

			Request::current()->redirect('akills/view/'.$t.($h!==null?'/'.$h:''));			
		}
	}

	public function action_completed() {
		$this->template->content = 'Your ticket has been filed, check your email for more details.';
	}
	
	public function action_assign($t) {
		if ($this->auth->getUser() === null) {
			$c = Request::current();
			$c->redirect('home/login?referrer='.$c->uri());
		}

		$ticket = new Model_Ticket();
		$user = new Model_User();
		
		$ti = $ticket->get($t);
		$ticket->assign($t, $_POST['assignee']==''?null:$_POST['assignee']);
		$mail = new Mail($ti['contact_name'], $ti['contact_email']);
		if ($_POST['assignee']=='') {
			if ($ti['status'] == 1) {
				$ticket->setStatus($t, 0);
				$ticket->addHistory($t, $this->auth->getUser(), 'Ticket unassigned and status switched to '.Ticket::status(0).'.');
			}
			$mail->ticket_unassigned($t, sha1($ti['ip'].$t));			
		} else {
			$u = $user->get($_POST['assignee']);
			if ($ti['status'] != 1) {
				$ticket->setStatus($t, 1);
				$ticket->addHistory($t, $this->auth->getUser(), 'Ticket assigned to '.$u['name'].', and status switched to '.Ticket::status(1).'.');
			} else {
				$ticket->addHistory($t, $this->auth->getUser(), 'Ticket assigned to '.$u['name'].'.');
			}
			$mail->ticket_assigned($t, sha1($ti['ip'].$t), $u['name']);
		}
		Request::current()->redirect('akills/view/'.$t);
	}

	public function action_json() {
		if (isset($_POST['password']) && sha1($_POST['password']) === Kohana::config('json.key')) {
			$history = new Model_History();
		
			echo json_encode($history->recent()->as_array());
		}
		$this->auto_render = false;
	}
	
} // End Akills
