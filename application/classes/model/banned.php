<?php 
class Model_Banned extends Model {

    public function check($ip) {
        $q = DB::query(Database::SELECT, 'SELECT 1 FROM banned WHERE ip = :ip LIMIT 1')->param(':ip', $ip);
        $r = $q->execute();
        if (count($r) > 0) {
            return true;
        }
        return false;
    }

    public function ban($ip)
    {
	    $q = DB::query(Database::INSERT, 'INSERT INTO banned (ip) VALUES (:ip)')->param(':ip', $ip);
	    return $q->execute();
    }

    public function unban($ip)
    {
	    $q = DB::query(Database::DELETE, 'DELETE FROM banned WHERE ip = :ip')->param(':ip', $ip);
	    return $q->execute();
    }
}
