<?php
class Model_User extends Model {

	public function get($u) {
		$q = DB::query(Database::SELECT, 'SELECT user, name, staff FROM user WHERE user = :u')->param(':u', (int) $u);
		$r = $q->execute();
		if (count($r) > 0) {
			return $r[0];
		} else {
			return null;
		}
	}
	
	public function getByName($n) {
		$q = DB::query(Database::SELECT, 'SELECT user, name, staff FROM user WHERE name = :n')->param(':n', $n);
		$r = $q->execute();
		if (count($r) > 0) {
			return $r[0];
		} else {
			return null;
		}
	}
	
	public function create($name) {
		$q = DB::query(Database::INSERT, 'INSERT INTO user (name, staff) VALUES (:n, 1)')->param(':n', $name);
		return $q->execute();
	}

	public function staff() {
		$q = DB::query(Database::SELECT, 'SELECT user, name, staff FROM user WHERE staff = 1');
		return $q->execute();
	}

	public function vb3($username, $password) {
		$q = DB::query(Database::SELECT, "SELECT password, salt, usergroupid, membergroupids FROM user WHERE username = :username LIMIT 1")->param(':username', $username);
		$r = $q->execute('vb');
		if (count($r) > 0) {
			if ($r[0]['password'] === md5(md5($password).$r[0]['salt'])) {
				$groups = explode(',', $r[0]['membergroupids']);
				$vb = Kohana::$config->load('database');
				$required_group_id = $vb['vb']['vb_group_id'];
				return in_array($required_group_id, $groups) || $r[0]['usergroupid'] == $required_group_id;
			}
		}
		return false;
	}
	
	public function vb3Email($username) {
        $q = DB::query(Database::SELECT, "SELECT email FROM user WHERE username = :username LIMIT 1")->param(':username', $username);		
		$r = $q->execute('vb');
        if (count($r) > 0) {
			return $r[0]['email'];
		}
		return false;
	}
}
