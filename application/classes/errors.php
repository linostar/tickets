<?php
class Errors {
	public static function display($e) {
		$s = '<ul class="errors">';
		switch ($e[0]) {
		case 'not_empty':
			$s .= '<li>Required.</li>';
			break;
		case 'email':
			$s .= '<li>Please enter a valid email.</li>';
			break;
		case 'max_length':
			$s .= '<li>Please shorten entry.</li>';
			break;
		case 'ip':
			$s .= '<li>Please enter a valid IP.</li>';
			break;
		default:
			$s .= '<li>Error</li>';
			break;
		}
		$s .= '</ul>';
		return $s;
	}
}
?>