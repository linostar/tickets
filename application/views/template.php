<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Rizon Abuse</title>
	<style type="text/css">
		html {
			padding: 0;
			margin: 0;
		}
		body {
			font: normal 13px "Lucida Sans Unicode", "Lucida Sans", Verdana, Arial, sans-serif;
			color: #222222;
		 	padding: 0;
			margin: 0;
		}
		ul, li {
			padding: 0;
			margin: 0;
			list-style: none;
			display: table;
		}
		h2 {
			margin: 1em 0 0.5em 0;
			font: bold 18px Arial, Verdana, sans-serif;
		}
		h3 {
			padding: 0;
			margin: 2em 0 0.5em 0;
			font: bold 17px Arial, Verdana, sans-serif;
		}
		img {
			border: 0;
		}
		#header {
		    background-color:#f3f3f3;
			background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#fafafa), to(#eaeaea));
			background-image: -moz-linear-gradient(top, #fafafa, #eaeaea);
			filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#fafafa', EndColorStr='#eaeaea');
			height: 110px;
		}
		#headerInner {
			position: relative;
			padding-top: 27px;
			margin: 0 auto;
			min-width: 800px;
			_width: 900px;
			max-width: 900px;
		}
		#userBar {
			position: absolute;
			top: 0;
			right: 0;
			text-align: right;
			padding-top: 10px;
			text-transform: lowercase;
		}
		#userBar a {
			color: #222222;
		}
		#navigation {
			text-transform: lowercase;
			position: absolute;
			right: 0;
			top: 50px;
			font-size: 14px;
		}
		#navigation li {
			float: left;
			padding-left: 2em;
		}
		#navigation a {
			display: block;
			padding: 0 0.5em;
			text-decoration: none;
			color: #222222;
			border-radius: 3px;
		    border:1px solid #f3f3f3;
		}
		#navigation a:hover {
		    color:#0d4668;
		    border-color:#98bdd2;
		    background-color:#daf2ff;
		    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#daf2ff), to(#b9e5ff));
		    background-image: -moz-linear-gradient(top, #daf2ff, #b9e5ff);
		    filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#daf2ff', EndColorStr='#b9e5ff');
		}
		#navigation .active a {
			font-weight: bold;
			color: #222222;
		    border-color:#aeaeae;
		    background-color:#dadada;
		    background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(#e8e8e8), to(#c7c7c7));
		    background-image: -moz-linear-gradient(top, #e8e8e8, #c7c7c7);
		    filter: progid:DXImageTransform.Microsoft.Gradient(GradientType=0, StartColorStr='#e8e8e8', EndColorStr='#c7c7c7');
		}
		#navigation .active a:hover {

		}
		#content {
			margin: 0 auto;
			min-width: 800px;
			_width: 900px;
			max-width: 900px;
			padding: 20px 0 0 0;
		}
		#footer {
			margin: 0 auto;
			max-width: 900px;
			_width: 900px;
			min-width: 800px;
			text-align: right;
			font-size: 0.85em;
			color: #aaaaaa;
			padding-bottom: 20px;
		}


		#submit_appeal, #submit_appeal li {
			margin: 0;
			padding: 0;
			list-style: none;
		}
		#submit_appeal li {
			clear: left;
			padding-top: 0.25em;
			min-height: 1.5em;
		}
		#submit_appeal label {
			display: block;
			width: 11em;
			float: left;
			font-style: normal;
		}
		#user {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			text-align: right;
		}

		table {
			width: 100%;
			border-collapse: collapse;
			border: 1px solid #cccccc;
		}
		table td, table th {
			border: 1px solid #cccccc;
			padding: 0.2em 0.5em;
		}
		table tr:nth-child(2n) {
  			background-color: #fafafa;
		}
		table.tickets tr:hover td {
			background-color: #daf2ff;
			cursor: pointer;
		}
		table th {
			background: #eeeeee;
		}
		#historyTime {
			width: 12em;
		}
		#historyUser {
			width: 13em;
		}

		.tickets td {
			font-size: 0.95em;
		}
		.tickets .closed, .tickets .closed a {
			color: #666666;
		}

		.tickets .date, .tickets .status {
			white-space: nowrap;
		}


		.info li {
			padding: 0.3em 0;
		}
		.info em {
			font-weight: bold;
			font-style: normal;
		}
		.infoa {
			width: 1em;
			white-space: nowrap;
			font-weight: bold;
			color: #444444;
			background: #eeeeee;
		}
		#replies li {
			background: #eeeeee;
			border: 1px solid #cccccc;
			padding: 1em 1em;
			margin-bottom: 1em;
		}
		#replies p {
			margin: 0 0 1em 0;
		}
		#recaptcha_table {
			width: auto;
		}
		#replyHeader {
			display: none;
		}

		fieldset {
			border: 1px solid #cccccc;
			padding: 1em;
		}
		fieldset legend {
			font-weight: bold;
		}
		#login label {
			display: block;
			float: left;
			width: 5.5em;
			padding-top: 0.3em;
		}
		.errors {
			margin-top: 0;
			margin-bottom: 1.5em;
		}
		.errors li {
			background: #FFCCBB;
			padding: 0.5em;
			margin-top: 0;
			margin-bottom: 1em;
		}
		#submit_appeal .errors li {
			padding: 0.5em;
			margin-bottom: 1em;
		}
		h1 {
			padding: 0;
			margin: 0;
		}
		h1 a:hover {
			opacity: 0.7;
		}
		h1 a:active {
			opacity: 1;
		}
	</style>
</head>
<body>
	<div id="header">
		<div id="headerInner">
			<h1><a href="http://rizon.net"><img src="<?php echo URL::base(); ?>images/logo.png" alt="Rizon Chat Network"></a></h1>
			<div id="userBar">
				<?php if (Auth::instance()->getUser() !== null) { ?>
					<a href="<?php echo URL::site('admin'); ?>">Admin</a> | <a href="<?php echo URL::site('search'); ?>">Search</a> | <a href="<?php echo URL::site('home/logout'); ?>">Logout</a>
				<?php } else { ?>
					<a href="<?php echo URL::site('home/login'); ?>">Staff Login</a>
				<?php } ?>
			</div>
			<?php
				class Navigation {
					private $items = array();
					private $ulId = '';
					private $activeClass = '';
					public function __construct($ulId, $activeClass, $currentController, $currentAction) {
						$this->ulId = $ulId;
						$this->activeClass = $activeClass;
						$this->currentController = $currentController;
						$this->currentAction = $currentAction;
					}
					public function generateHtml() {
						$r = '';
						$r .= '<ul id="'.$this->ulId.'">';
						foreach ($this->items as $v) {
							$location = $v[0];
							$name = $v[1];
							$isActive = false;
							if (count($location) === 1) {
								$isActive = $location[0] === $this->currentController;
							} else if (count($location) >= 2) {
								$isActive = $location[0] === $this->currentController && $location[1] == $this->currentAction;
							}
							$r .= '<li'.($isActive?' class="'.$this->activeClass.'"':'').'><a href="'.url::site(implode('/', $location)).'">'.$name.'</a></li>';
						}
						$r .= '</ul>';
						return $r;
					}
					public function addItem(array $location, $name) {
						$this->items[] = array($location, $name);
					}
				}
				$n = new Navigation('navigation', 'active', Request::current()->controller(), Request::current()->action());
				$n->addItem(array('home'), 'Abuse Home');
				$n->addItem(array('akills'), 'Bans');
				echo $n->generateHtml();
			?>
		</div>
	</div>
	<div id="content">
		<?php
		echo $content;
		?>
	</div>
	<div id="footer">
		&copy; rizon.net
	</div>
</body>
</html>
