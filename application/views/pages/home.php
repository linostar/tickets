<h2>Rizon Abuse</h2>
<p>Welcome to the Rizon abuse website! This site has been setup to help you contact the K-Line team in regards to your ban.</p>
<p>Our main goal is to keep Rizon <em>clean</em>, which is not <em>only</em> a job for our K-Line team but for <em>every</em> member of the Rizon staff. </p>
<h3>Automatic K-Lines</h3>
<p>You may have been automatically banned from Rizon. Click on the "bans" link in the navigation above to a) check your current ban status; and/or b) submit an appeal.</p>
<p><small>Note: Upon connection to Rizon we may scan your IP for open proxies using our proxy monitor. Scans from the BOPM are harmless and will come from scanner.rizon.net.</small></p>
