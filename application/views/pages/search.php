<form action="<?php echo URL::site('search') ?>" method="post" id="search">
	<p>Terms will be as-is, so spaces do not separate search terms. Only the opening ticket will be searched, not the replies to it. Only the IP, contact name, contact e-mail, ban message and "why" fields are searched.</p>
	<p>Prefix your search with &amp; to force exact matches. This is probably not very useful for the "why" field, however.</p>
	<input name="term" type="text" value="<?php echo(isset($last_term) ? $last_term : ''); ?>">
	<input type="submit" value="Search">
</form>

<?php
if (isset($results)) {
	echo View::factory('ticket_list')->bind('tickets', $results)->set('ticket_section_title', 'Search results')->set('ticket_status_name', "found for: $last_term");
} // isset($results)
?>

