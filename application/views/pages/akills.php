<?php 
if (!isset($post)) {
	$post = array();
}
if (!isset($errors)) {
	$errors = array();
}

function displayForm($ip, $post, $errors, $akill, $allowAppeal) {
?>
<h2>Submit a Ticket</h2>
<script type="text/javascript">
var RecaptchaOptions = {
	theme : 'clean'
};
</script>
<form action="<?php echo URL::site('akills/appeal'); ?>" method="post">
	<ul id="submit_appeal">
		<li><label for="ip">IP</label><?php echo HTML::chars($ip); ?><input type="hidden" name="ip" value="<?php echo HTML::chars($ip); ?>"><input type="hidden" name="type" value="<?php echo isset($_GET['type']) ? HTML::chars($_GET['type']) : HTML::chars($post['type']); ?>"></li>
		<?php
		if (isset($akill)) {
		?>
		<li><label>Ban Information</label>
		<?php
		if ($akill === false) {
			echo 'No information found.';
		} else {
			echo '<table class="info" style="width: auto">';
			echo '<tr><td class="infoa">Timestamp</td><td>'.HTML::chars($akill['timestamp']).'</td></tr>';
			echo '<tr><td class="infoa">Duration</td><td>'.HTML::chars(Time::relative($akill['time'])).'</td></tr>';
			echo '<tr><td class="infoa">Message</td><td>'.HTML::chars($akill['message']).'</td></tr>';
			echo '</table>';
		}
		?>
		</li>
		<?php } ?>
		
		
		<?php
		if ($allowAppeal) {
		?>
		<li><label for="contact_name">Contact Name</label>
			<input type="text" name="contact_name" id="contact_name" value="<?php echo isset($post['contact_name']) ? HTML::chars($post['contact_name']) : ''?>">
			<?php 
			if (isset($errors['contact_name'])) {
				echo Errors::display($errors['contact_name']);
			}
			?>
		</li>
		<li><label for="contact_email">Contact Email</label>
			<input type="text" name="contact_email" id="contact_email" value="<?php echo isset($post['contact_email']) ? HTML::chars($post['contact_email']) : ''?>">
			<?php 
			if (isset($errors['contact_email'])) {
				echo Errors::display($errors['contact_email']);
			}
			?>
		</li>
		
		
		
		<?php 
			if (!isset($akill)) {
		?>
		<li>
			<label for="host_number">Number of Hosts</label>
			<input type="text" name="host_number" id="host_number" size="3" value="<?php echo isset($post['host_number']) ? HTML::chars($post['host_number']) : ''?>">
			<?php 
			if (isset($errors['host_number'])) {
				echo Errors::display($errors['host_number']);
			}
			?>
		</li>
		<?php
			} else if ($akill === false) {
		?>
		<li><label for="ban_message">Ban Message</label>
			<textarea name="ban_message" id="ban_message" rows="7" cols="53"><?php echo isset($post['ban_message']) ? HTML::chars($post['ban_message']) : ''?></textarea>
			<?php 
			if (isset($errors['ban_message'])) {
				echo Errors::display($errors['ban_message']);
			}
			?>
		</li>
		<?php						
			}
		?>
		
		
		<li><label for="why">Why</label>
			<textarea name="why" id="why" rows="7" cols="53"><?php echo isset($post['why']) ? HTML::chars($post['why']) : ''?></textarea>
			<?php 
			if (isset($errors['why'])) {
				echo Errors::display($errors['why']);
			}
			?>
		</li>
		
		
		<?php if (!isset($akill)) { ?>
		<li><label for="static">My IP Doesn't Change</label>
			<input type="checkbox" name="static" id="static" value="yes" <?php echo isset($post['static']) ? 'checked="checked"' : '' ?>>
			<?php 
			if (isset($errors['static'])) {
				echo Errors::display($errors['static']);
			}
			?>
		</li>
		<?php } ?>
		
		
		<li><label>Human Verification</label>
			<?php echo Recaptcha::instance()->get_html(true); ?>
			<?php 
			if (isset($errors['recaptcha_response_field'])) {
				echo Errors::display($errors['recaptcha_response_field']);
			}
			?>
		</li>
		
		
		<?php 
		}
		?>
	</ul>
	<?php
	if ($allowAppeal) { ?>
	<input type="submit" value="Appeal">
	<p>Note: You will be contacted by email regarding the status of your appeal.</p>
	<?php } else {
	
		if (isset($_GET['type']) && $_GET['type'] === 'sli' || isset($post['type']) && $post['type'] === 'sli') {
		?>
		<p>A ticket already exists for this IP.</p>
		<?php
		} else {
		?>
		<p>Either the ban is less than 1 hr in length, or a ticket already exists in the abuse system for this ip.</p>
	<?php
		}
	} ?>
</form>
<?php
}

if (!isset($lookup_post)) {
	$lookup_post = array();
}
if (!isset($lookup_errors)) {
	$lookup_errors = array();
}
?>
<h2>Create Ticket</h2>
<form action="<?php echo URL::site('akills/lookup'); ?>" method="get">
	<p><label>Type</label>: <input type="radio" name="type" id="type_sli" value="sli" <?php echo isset($lookup_post['type']) && $lookup_post['type'] === 'sli'? 'checked="checked"' : ''; ?>> Session Limit Increase <input type="radio" name="type" id="type_akill" value="akill" <?php echo !(isset($lookup_post['type']) && $lookup_post['type'] === 'sli')? 'checked="checked"' : ''; ?>> Ban Appeal</p>
	<?php 
	if (isset($lookup_errors['type'])) {
		echo Errors::display($lookup_errors['type']);
	}
	?>
	<p><label for="ip">IP</label>: <input type="text" name="ip" id="id" value="<?php echo isset($lookup_post['ip']) ? HTML::chars($lookup_post['ip']) : $_SERVER['REMOTE_ADDR'] ?>"></p>
	<?php 
	if (isset($lookup_errors['ip'])) {
		echo Errors::display($lookup_errors['ip']);
	}
	?>

	<p><input type="submit" value="Begin ticket"></p>
</form>
<?php
if (isset($allowAppeal)) {
		displayForm($lookup_post['ip'], $post, $errors, isset($akill)?$akill:null, $allowAppeal);
}
?>
