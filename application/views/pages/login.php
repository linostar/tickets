<?php
if (!isset($errors)) {
	$errors = array();
}
?>
<form action="<?php echo URL::site('home/login?referrer='.html::chars($referrer)); ?>" method="post" id="login">
	<fieldset>
		<legend>Staff Login</legend>
		<ul>
			<li>
				<label for="username">Username</label>
				<input name="username" value="<?php echo html::chars($input['username']); ?>" type="text" id="username">
				<?php 
				if (isset($errors['username'])) {
					echo Errors::display($errors['username']);
				}
				?>
			</li>
			<li>
				<label for="password">Password</label>
				<input name="password" value="<?php echo html::chars($input['password']); ?>" type="password" id="password">
				<?php 
				if (isset($errors['password'])) {
					echo Errors::display($errors['password']);
				}
				?>
			</li>
		</ul>
		<input type="submit" value="Login">
	</fieldset>
</form>
	
