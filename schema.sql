DROP TABLE IF EXISTS `ticket_history`, `ticket_reply`, `ticket`, `user`, `akill`, `banned`;

CREATE TABLE `user` (
       `user` INT UNSIGNED NOT NULL auto_increment,
       `name` VARCHAR(255) NOT NULL,
       `staff` TINYINT(1) NOT NULL,
       PRIMARY KEY (`user`),
       KEY (`staff`)
) ENGINE=InnoDB;

CREATE TABLE `akill` (
       `akill` INT UNSIGNED NOT NULL auto_increment,
       `remote_id` INT NOT NULL,
       `ip` VARCHAR(70) NOT NULL,
       `time` VARCHAR(20) NOT NULL,
       `message` VARCHAR(200) NOT NULL,
       `addedby` VARCHAR(40) NOT NULL,
       `timestamp` DATETIME NOT NULL,
       `akillid` VARCHAR(13) NOT NULL default 'XX',
       PRIMARY KEY  (`akill`),
       KEY `ip_timestamp` (`ip`, `timestamp`)
) ENGINE=InnoDB;

CREATE TABLE `ticket` (
       `ticket` INT UNSIGNED NOT NULL auto_increment,
       `ip` VARCHAR(46) NOT NULL,
       `real_ip` VARCHAR(70) NOT NULL,
       `contact_name` VARCHAR(255) NOT NULL,
       `contact_email` VARCHAR(255) NOT NULL,
       `ban_message` TEXT NOT NULL,
       `host_number` SMALLINT UNSIGNED NOT NULL,
       `why` TEXT NOT NULL,
       `added` DATETIME NOT NULL,
       `status` TINYINT UNSIGNED NOT NULL,
       `assignee` INT UNSIGNED,
       `akill` INT UNSIGNED,
       `last_replier` VARCHAR(255) NOT NULL,
       `last_reply_at` DATETIME,
       PRIMARY KEY (`ticket`),
       FOREIGN KEY (`assignee`) REFERENCES `user` (`user`),
       FOREIGN KEY (`akill`) REFERENCES `akill` (`akill`),
       KEY (`status`),
       KEY (`ip`)
) ENGINE=InnoDB;
-- ALTER TABLE `ticket` ADD COLUMN `host_number` SMALLINT UNSIGNED NOT NULL AFTER `ban_message`
-- ALTER TABLE `ticket` ADD COLUMN `real_ip` VARCHAR(70) NOT NULL AFTER `ip`

CREATE TABLE `ticket_reply` (
       `ticket_reply` INT UNSIGNED NOT NULL auto_increment,
       `ticket` INT UNSIGNED NOT NULL,
       `message` TEXT NOT NULL,
       `at` DATETIME NOT NULL,
       `ip` VARCHAR(70) NOT NULL,
       `by` INT UNSIGNED,
       PRIMARY KEY (`ticket_reply`),
       FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`),
       FOREIGN KEY (`by`) REFERENCES `user` (`user`)
) ENGINE=InnoDB;

CREATE TABLE `ticket_history` (
       `ticket_history` INT UNSIGNED NOT NULL auto_increment,
       `ticket` INT UNSIGNED NOT NULL,
       `message` TEXT,
       `at` DATETIME,
       `by` INT UNSIGNED,
       PRIMARY KEY (`ticket_history`),
       FOREIGN KEY (`ticket`) REFERENCES `ticket` (`ticket`),
       FOREIGN KEY (`by`) REFERENCES `user` (`user`)
) ENGINE=InnoDB;

CREATE TABLE `banned` (
       `banned` INT UNSIGNED NOT NULL auto_increment,
       `ip` VARCHAR(70) NOT NULL,
       PRIMARY KEY (`banned`),
       UNIQUE KEY (`ip`)
) ENGINE=InnoDB;

